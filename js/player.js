class Player{
    constructor(width, height) {
        this.data = {}
        this.width = width // canvas rect
        this.height = height
        //
        
    }
    //
    add_player(name) {
        this.data[name] = {x:this.width/2, y:this.height/2, alive:true}
        // this.data['david'] = {x:this.width/3, y:this.height/3, alive:true}
        // this.data['john'] = {x:this.width/4, y:this.height/4, alive:false}
    }
    // die
    die(name) {
        if(this.data[name].alive) this.data[name].alive = false
    }
    // player move
    move(name, direction, delta) {
        switch(direction){
            case 'UP_RIGHT':
                this.data[name].x += 5
                this.data[name].y -= 5
            break
            case 'UP_LEFT':
                this.data[name].x -= 5
                this.data[name].y -= 5
            break
            case 'DOWN_RIGHT':
                this.data[name].x += 5
                this.data[name].y += 5
            break
            case 'DOWN_LEFT':
                this.data[name].x -= 5
                this.data[name].y += 5
            break
            case 'DOWN':
                this.data[name].y += 5
            break
            case 'UP':
                this.data[name].y -= 5
            break
            case 'LEFT':
                this.data[name].x -= 5 
            break
            case 'RIGHT':
                this.data[name].x += 5 
            break
        }
        // check if out of box
        if(this.data[name].x - 20 <=0 ){
            this.data[name].x = 20
        }
        if(this.data[name].x >= this.width ){
            this.data[name].x = this.width - 2
        }
        if(this.data[name].y - 20 <= 0) {
            this.data[name].y = 20
        }
        if(this.data[name].y  >= this.height){
            this.data[name].y = this.height 
        }
    }
}