$(document).ready(()=>{
    const canvas = new Canvas('mycanvas','mydiv')
    let name = prompt('Provide a name (6 Characters Max)','')
    if(name){
        if(name.length > 6) name = name.substring(0, 6)
        canvas.setName(name)
        canvas.init()
        canvas.gameStart()
    }
})

