class Canvas{
    //
    constructor(dom, div) {
        // basic setting
        this.deltatime = 0
        this.lasttime = Date.now()
        this.ball_time = 0  // timer for ball to be added
        this.ball_addtime = 10000
        this.ball_add = 5
        this.ball_num_init = 5
        // time
        this.sec = 0
        this.secR = 0
        //ball and player object
        this.ball
        this.player
        // get the style
        let temp = getComputedStyle(document.getElementById(div))
        this.width = temp.width.substr(0, temp.width.length-2)
        this.height = temp.height.substr(0, temp.height.length-2)
        let left = temp.left
        // mycanvas set
        this.dom = document.getElementById(dom)
        this.ctx = this.dom.getContext('2d')
        this.dom.width = this.width
        this.dom.height = this.height
        this.dom.style.left = left
        // add my player into array
        this.myplayer
        this.gameOver = false
        //
    }
    // init
    init() {
        this.ball = new Ball(this.width, this.height)
        this.player = new Player(this.width, this.height)
        // create my character
        this.player.add_player(this.myplayer)
        // create some balls first
        this.ball.create(this.ball_num_init)
        // bind key
        this.keyBind()
        //
        this.initPressKey()
    }
    // set name
    setName(name){
        // save name
        this.myplayer = name
    }
    // init press key
    initPressKey() {
        this.Key = {
            UP: false,
            DOWN: false,
            LEFT: false,
            RIGHT: false
        };
          
        this.DIRECTION = {
            87: "UP",
            68: "RIGHT",
            83: "DOWN",
            65: "LEFT"
        };
    }
    // game start
    gameStart() {
        // start the timer for draw all
        this.draw_all()
    }
    // key bind
    keyBind() {
        
        document.onkeydown = e =>{
            if(this.DIRECTION[e.keyCode]) {
                this.Key[this.DIRECTION[e.keyCode]] = true; 
            }
        }

        document.onkeyup = e => {
            if(this.DIRECTION[e.keyCode]){
                this.Key[this.DIRECTION[e.keyCode]] = false;
            }
        }
    }
    move(){
        // base on key to determind which way
        if(this.Key['UP'] && this.Key['RIGHT']) {
            this.player.move(this.myplayer, 'UP_RIGHT')
        }else if(this.Key['UP'] && this.Key['LEFT']) {
            this.player.move(this.myplayer, 'UP_LEFT')
        }else if(this.Key['DOWN'] && this.Key['LEFT']) {
            this.player.move(this.myplayer, 'DOWN_LEFT')
        }else if(this.Key['DOWN'] && this.Key['RIGHT']) {
            this.player.move(this.myplayer, 'DOWN_RIGHT')
        }else if(this.Key['UP']) {
            this.player.move(this.myplayer, 'UP')
        }else if(this.Key['DOWN']) {
            this.player.move(this.myplayer, 'DOWN')
        }else if(this.Key['LEFT']) {
            this.player.move(this.myplayer, 'LEFT')
        }else if(this.Key['RIGHT']) {
            this.player.move(this.myplayer, 'RIGHT')
        }
    }

    // full draw
    draw_all() {
        // check if gameover 
        if(this.gameOver){
            this.gameover_it()
        }
        // time set
        let now = Date.now()
        this.deltatime = now - this.lasttime
        this.lasttime = now
        if(this.deltatime>50) this.deltatime=50
        //clear
        this.ctx.clearRect(0, 0, this.width, this.height)
        // draw
        this.add_ball()
        this.draw_ball()
        this.draw_player()
        this.draw_time()
        this.move();
        this.playerHit()
        // set
        window.requestAnimationFrame(()=>{
            this.draw_all()
        })
    }

    // draw the time out
    draw_time(){
        this.secR += this.deltatime
        if(this.secR >= 1000){
            this.secR -= 1000
            this.sec += 1
        }
        let w = this.width
        let h = this.height
        this.ctx.save()
        this.ctx.font = '30px Verdana' 
        this.ctx.textAlign = 'center'
        this.ctx.shadowBlur = 10  
        this.ctx.shadowColor = 'red' 
        this.ctx.fillStyle = 'white'
        // 绘制出文字
        this.ctx.fillText('TIME: '+this.sec, w * 0.5, h - 20)
        this.ctx.restore()
    }
    
    // check player if hit balls
    playerHit() {
        // check if player die
        let p = this.player.data[this.myplayer]
        console.log(this.player.data)
        if(p['alive']){
            //
            for(let val of this.ball.data){
                // check
                let cal = this.RectCircleColliding(
                    {x:val.x,y:val.y,r:val.radius}, 
                    {x:p.x-10,y:p.y-10,w:20,h:20})
                // if it collding
                if (cal) {
                    this.player.data[this.myplayer].alive = false
                    this.gameover_it()
                }
            }
        }
    }

    // rect and circle colliding
    RectCircleColliding(circle,rect){
        var distX = Math.abs(circle.x - rect.x-rect.w/2);
        var distY = Math.abs(circle.y - rect.y-rect.h/2);
    
        if (distX > (rect.w/2 + circle.r)) { return false; }
        if (distY > (rect.h/2 + circle.r)) { return false; }
    
        if (distX <= (rect.w/2)) { return true; } 
        if (distY <= (rect.h/2)) { return true; }
    
        var dx=distX-rect.w/2;
        var dy=distY-rect.h/2;
        return (dx*dx+dy*dy<=(circle.r*circle.r));
    }
    
    gameover_it() {
        document.onkeydown = null
        document.onkeyup = null
        this.player.die(this.myplayer)
        this.initPressKey()
    }

    //add ball into game
    add_ball() {
        this.ball_time += this.deltatime
        if(this.ball_time >= this.ball_addtime) {
            this.ball_time -= this.ball_addtime
            this.ball.create(this.ball_add)
        }
    }

    // draw player
    draw_player() {
        this.ctx.save()
        this.ctx.lineWidth=5;
        this.ctx.strokeStyle='black';
        // for name
        this.ctx.font = '10px Verdana' 
        this.ctx.textAlign = 'center'
        //
        for(let key in this.player.data){
            let val = this.player.data[key]
            this.ctx.save()
            //
            this.ctx.beginPath();
            this.ctx.translate(val.x, val.y)
            if(key == this.myplayer){ // check if is my character
                this.ctx.fillStyle='orange';
            }else{
                this.ctx.fillStyle='#f00';
            }
            // check if alive
            if(!val.alive) {
                this.ctx.fillStyle = 'gray'
            }
            // text
            this.ctx.save()
            if(!val.alive) {
                this.ctx.fillStyle = 'gray'
            }else{
                this.ctx.fillStyle = 'yellow'
            }
            this.ctx.fillText(key, -10, 10)
            this.ctx.restore()
            //
            this.ctx.rect(-20, -20, 20, 20);
            //
            this.ctx.stroke();
            this.ctx.fill();
            // draw eyes
            this.ctx.beginPath();
            this.ctx.fillStyle = 'white'
            this.ctx.arc(-14, -13, 3, 0, Math.PI * 2)
            this.ctx.arc(-6, -13, 3, 0, Math.PI * 2)
            this.ctx.fill();
            //
            this.ctx.restore()
        }
        //
        this.ctx.restore()
    }

    // draw ball out
    draw_ball() {
        this.ctx.save()
        this.ctx.lineWidth = 3 
        this.ctx.shadowBlur = 10
        this.ctx.shadowColor = 'white'
        //
        for(let [key,val] of this.ball.data.entries()){
            // move
            this.ball.data[key].x += val.x_move * (this.deltatime/20)
            this.ball.data[key].y += val.y_move * (this.deltatime/20)
            // check if touch edge
            let change = this.ball.check_touch(key)
            if(change){
                // change speed
                this.ball.change_speed(change, key)
                // change color
                this.ball.change_color(key)
                // change radius
                this.ball.change_radius(key)
            }
            // if ball touch then change 
            this.ball.data[key]
            // draw
            this.ctx.beginPath()
            this.ctx.arc(val.x, val.y, val.radius, 0, Math.PI * 2)
            this.ctx.closePath()
            this.ctx.fillStyle = `rgba(${val.color}, 1)`
            this.ctx.fill()
        }
        //
        this.ctx.restore()
    }
}